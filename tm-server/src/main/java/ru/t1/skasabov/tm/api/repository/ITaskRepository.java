package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @Nullable
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
