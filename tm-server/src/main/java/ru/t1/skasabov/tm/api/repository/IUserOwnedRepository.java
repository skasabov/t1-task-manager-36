package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    Boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M add(@NotNull String userId, @Nullable M model);

    @Nullable
    M removeOne(@NotNull String userId, @Nullable M model);

    void removeAll(@Nullable String userId);

}
