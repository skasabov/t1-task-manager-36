package ru.t1.skasabov.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserProfileRequest extends AbstractUserRequest {
}
